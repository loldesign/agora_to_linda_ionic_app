angular.module('starter')
  .directive('validate', function (validation, $filter) {
  
  return function(scope, element, attr){
    element.bind("keyup", function() {
      
      var s = angular.element(element).val();
      var type = angular.element(element).attr("validate");

      switch(type){
        case "month" : 
          if(!validation.monthYear(s, null)){
            if(angular.element(element).parent().find('div').hasClass('error-now')){
              angular.element(element).parent().find('div').html("Campo incorreto verfique!");
            }
          }else{
            angular.element(element).parent().find('div').html("");
          }          
        break;

        case "date" : 
          if(!validation.date(s,false)){
            if(angular.element(element).parent().find('div').hasClass('error-now')){
              angular.element(element).parent().find('div').html("Campo incorreto verfique!");
            }
          }else{
            angular.element(element).parent().find('div').html("");
          }          
        break;

        case "presence" : 
          if(s == null || s == "" || s == undefined){

            if(angular.element(element).parent().find('div').hasClass('error-now')){
              angular.element(element).parent().find('div').html("Campo incorreto verfique!");
            }
          }else{
            angular.element(element).parent().find('div').html("");
          }          
        break;

      }

    });    
  }
  
});