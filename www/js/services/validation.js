angular.module('starter')
  .service('validation', function($filter) {
        
    this.date = function(data, compare){

      var temp = $filter('date')(data, "dd/MM/yyyy");

      if(temp != null && temp != "" && temp != undefined){
        var regex = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/[0-9]{4}$/;

        if(compare){
          if($filter('date')(data, "yyyyMM") > $filter('date')(new Date(), "yyyyMM")){
            return false;
          }
        }

        if(temp.match(regex)){
          return true;
        }

       
      }

      return false;

    },
    this.dateCompare = function(data1, data2){

      if($filter('date')(data1, "yyyyMM") < $filter('date')(data2, "yyyyMM") && $filter('date')(new Date(), "yyyyMM") < $filter('date')(data2, "yyyyMM")){
        return true;
      }

      return false;
    },
    this.monthYear = function(data, data2){

      if(data2 != null){
        if($filter('date')(data2, "yyyyMM") > $filter('date')(new Date(), "yyyyMM")){
          return false;
        }
      }

      if(data != null && data != "" && data != undefined){
        var regex = /^(0[1-9]|1[012])\/\d{4}$/;

        if(data.match(regex)){
          return true;
        }
      }

      

      return false;

    }
    
  });