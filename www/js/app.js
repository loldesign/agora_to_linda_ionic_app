// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'dpd'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.value('dpdConfig', { 
    collections: ['users'], 
    serverRoot: 'http://agoratolinda.api.loldesign.com.br/', // optional, defaults to same server
    socketOptions: { reconnectionDelayMax: 3000 }, // optional socket io additional configuration
    useSocketIo: true, // optional, defaults to false
    noCache: true, // optional, defaults to false (false means that caching is enabled, true means it disabled)
    useSocketIo: false
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('login', {
    url: "/login",
    abstract: true,
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'
  })

  .state('login.singin', {
    url: "/sing-in",
    views: {
      'menuContent': {
        templateUrl: "templates/singin.html",
        controller: 'SinginCtrl'
      }
    }
  })

  .state('login.singup', {
    url: "/sing-up",
    views: {
      'menuContent': {
        templateUrl: "templates/singup.html",
        controller: 'SingupCtrl'
      }
    }
  })

  .state('app.completeinfo', {
    url: "/complete-info",
    views: {
      'menuContent': {
        templateUrl: "templates/completeinfo.html",
        controller: 'CompleteinfoCtrl'
      }
    }
  })

  .state('app.search', {
    url: "/search",
    views: {
      'menuContent': {
        templateUrl: "templates/search.html"
      }
    }
  })

  .state('app.browse', {
    url: "/browse",
    views: {
      'menuContent': {
        templateUrl: "templates/browse.html"
      }
    }
  })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login/sing-in');
});
