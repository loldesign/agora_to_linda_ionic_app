angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, dpd) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/mlogin.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('LoginCtrl', function($scope, $ionicModal, $timeout, dpd) {
  
})

.controller('CompleteinfoCtrl', function($scope, $state, $stateParams, dpd, $http, utils, $ionicPopup) {
  
  

})

.controller('SingupCtrl', function($scope, $state, $stateParams, dpd, $http, utils, $ionicPopup) {
  
  $scope.login = {'term' : false};

  function show_error(text){
    
    var alertPopup = $ionicPopup.alert({
       title: 'Erro',
       template: text
     });
     alertPopup.then(function(res) {
       
     });

    return false;      
  }

  $scope.appLogin = function(login){
    console.log(login.term);
    if(utils.nil(login)){
      show_error("Todos os Campos são Obrigatórios");

      return false;
    }

    if(utils.nil(login.email)){
      show_error("E-mail é Obrigatórios");

      return false;
    }

    if(utils.nil(login.username)){
      show_error("Login é Obrigatórios");

      return false;
    }

    if(utils.nil(login.password)){
      show_error("senha é Obrigatórios");

      return false;
    }

    if(utils.nil(login.repeat_password)){
      show_error("repita senha é Obrigatórios");

      return false;
    }

    if(login.password != login.repeat_password){
      show_error("senhas não correspondem");

      return false;
    }

    if(login.term == false){
      show_error("deve aceitar os termos");

      return false;
    }

    var request = $http({
        method: 'POST',
        url: "http://agoratolinda.api.loldesign.com.br/users",
        data: {
                "username": login.username, 
                "password": login.password
              },
        headers: {'Content-Type': 'application/json'}
    });
 
    request.success(
      function( data ) {
        console.log( data );

        login.username = "";
        login.password = "";

        $state.go("app.completeinfo");

      }
    );

    request.error(
      function( error ){

        var msg = "Erro ao cadastar usuário";

        if(error.status == 400){
          msg = "Usuário já existe";
        }

        show_error(msg);
      }
    );
 
  }

})

.controller('SinginCtrl', function($scope, $state, $stateParams, dpd, $http, utils, $ionicPopup) {

  var fbLogged = new Parse.Promise();

  if (!window.cordova) {
    facebookConnectPlugin.browserInit('871155056293186');
  }

  $scope.loginFacebook = function(){
    facebookConnectPlugin.login(['email'], fbLoginSuccess, fbLoginError);
  }

  var fbLoginError = function(error){
    fbLogged.reject(error);
  };

  var fbLoginSuccess = function(response) {

    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }
    var expDate = new Date(
      new Date().getTime() + response.authResponse.expiresIn * 1000
    ).toISOString();

    var authData = {
      id: String(response.authResponse.userID),
      access_token: response.authResponse.accessToken,
      expiration_date: expDate
    }
    fbLogged.resolve(authData);

    $state.go("app.completeinfo");
  };

  function show_error(text){
    
    var alertPopup = $ionicPopup.alert({
       title: 'Erro',
       template: text
     });
     alertPopup.then(function(res) {
       
     });

    return false;      
  }

  $scope.appLogin = function(login){

    if(utils.nil(login)){
      show_error("Todos os Campos são Obrigatórios");

      return false;
    }

    var request = $http({
        method: 'POST',
        url: "http://agoratolinda.api.loldesign.com.br/users/login",
        data: {
                "username": login.username, 
                "password": login.password
              },
        headers: {'Content-Type': 'application/json'}
    });
 
    request.success(
      function( data ) {
        console.log( data );

        login.username = "";
        login.password = "";

        $state.go("app.completeinfo");
      }
    );

    request.error(
      function( error ){
        show_error("Login ou senha inválidos");
      }
    );
 
  }
});
